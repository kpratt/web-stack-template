import { deepEqual } from 'assert';

describe("My sanity is in question", () => {

    afterEach(fetch.restore);

    it('Fetch is mocked.', (done) => {
        const jsonResponse = {
            cake: 'eat it you will',
        };
        fetch.get(
            'https://localhost',
            {
                status: 200,
                body: jsonResponse,
            }
        );
        fetch('https://localhost').
        then((resp) => resp.json()).
        then((json) => {
            deepEqual(jsonResponse, json);
            done();
        }).catch(done.fail);
    });

});