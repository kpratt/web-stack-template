# Dear Front End, get your shit together.

There is something deeply wrong about working in web front-end.
Something to do with having every bloody script kiddy in the world
writing their own solution for how to make the thing do the stuff
that results in a glorified mish mash of solutions all having to be
plug-able into the standard tool chain. Which inevitably ends up looking like this:

![Twenty plugs in a six plug power-bar](http://ta.inkserver.com.au/wp-content/uploads/2015/09/sep12_02_occupational2.jpg)

And you wonder why people want to do frontend coding in golang!
One day, open source willing, we'll have not just DOM bindings
but react bindings too.

## In the mean time

This repo contains little or no meaningful code. At most it contains
some boiler plate examples of how bits of the tech stack fit together,
including the test tools and tests that should accompany instances
of the tech stack.

**HOWEVER**, it WILL contain a working configuration that makes all these
bloody things **WORK!**

## The Stack

...the current plan is to include:

### Prod

* [Typescript](https://www.typescriptlang.org/)
* [React](https://reactjs.org/) UI
* [React Router](https://reacttraining.com/react-router/) (navigation component)
* [Redux](https://redux.js.org/docs/introduction/CoreConcepts.html) event logic
	* [rxjs](http://reactivex.io/rxjs/) / [redux-observable](https://github.com/redux-observable/redux-observable) event streams

### Testing
* [Jest](https://facebook.github.io/jest/) testing
* [sinon](http://sinonjs.org/) mocking
* [enzyme](https://github.com/airbnb/enzyme) component testing